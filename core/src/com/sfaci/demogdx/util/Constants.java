package com.sfaci.demogdx.util;

/**
 * Created by astable on 1/20/17.
 *
 * @author
 */
public class Constants {

    public final static String GAME_NAME = "com.sfaci.demogdx";

    public final static int SCREEN_WIDTH = 800;
    public final static int SCREEN_HEIGHT = 600;
}
