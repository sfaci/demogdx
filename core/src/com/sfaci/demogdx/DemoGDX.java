package com.sfaci.demogdx;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.sfaci.demogdx.screens.SplashScreen;

public class DemoGDX extends Game {

	@Override
	public void create () {
        setScreen(new SplashScreen());
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
	}
}
