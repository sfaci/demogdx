package com.sfaci.demogdx.screens;

import com.badlogic.gdx.*;
import com.sfaci.demogdx.DemoGDX;

/**
 * Created by astable on 1/20/17.
 *
 * @author
 */
public class SplashScreen implements Screen {

    @Override
    public void show() {

        ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen());
    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
