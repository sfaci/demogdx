package com.sfaci.demogdx.screens;

import com.badlogic.gdx.Screen;

/**
 * Created by astable on 1/20/17.
 *
 * @author
 */
public class GameOverScreen implements Screen {
    @Override
    public void show() {

    }

    @Override
    public void render(float dt) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
