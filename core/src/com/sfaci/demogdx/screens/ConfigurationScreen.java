package com.sfaci.demogdx.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.*;
import com.sfaci.demogdx.util.Constants;

/**
 * Created by astable on 1/20/17.
 *
 * @author
 */
public class ConfigurationScreen implements Screen {

    Stage stage;
    VisCheckBox soundCheckbox, musicCheckbox;
    VisSlider volumeSlider;
    VisTextButton doneButton;

    @Override
    public void show() {

        stage = new Stage();
        int yOffset = 0;

        VisTable table = new VisTable();
        table.setWidth(Constants.SCREEN_WIDTH);
        table.setHeight(Constants.SCREEN_HEIGHT);
        table.setFillParent(true);
        table.setPosition(0, 0);
        stage.addActor(table);

        soundCheckbox = new VisCheckBox("Music");
        soundCheckbox.setChecked(true);
        soundCheckbox.setWidth(200);
        soundCheckbox.setHeight(50);
        soundCheckbox.setPosition(Constants.SCREEN_WIDTH / 2 - soundCheckbox.getWidth() / 2, Constants.SCREEN_HEIGHT / 2);
        soundCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                checkVolumeSlider();
            }
        });
        table.addActor(soundCheckbox);

        musicCheckbox = new VisCheckBox("Sound effects");
        musicCheckbox.setChecked(true);
        musicCheckbox.setWidth(200);
        musicCheckbox.setHeight(50); yOffset += 60;
        musicCheckbox.setPosition(Constants.SCREEN_WIDTH / 2 - musicCheckbox.getWidth() / 2, Constants.SCREEN_HEIGHT / 2 - yOffset);
        musicCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                checkVolumeSlider();
            }
        });
        table.addActor(musicCheckbox);

        VisLabel volumeLabel = new VisLabel("Volume");
        volumeLabel.setWidth(200);
        volumeLabel.setHeight(10); yOffset += 15;
        volumeLabel.setPosition(Constants.SCREEN_WIDTH / 2 - volumeLabel.getWidth() / 2, Constants.SCREEN_HEIGHT / 2 - yOffset);
        table.addActor(volumeLabel);

        volumeSlider = new VisSlider(0, 255, 1, false);
        volumeSlider.setValue(100);
        volumeSlider.setWidth(200);
        volumeSlider.setHeight(50); yOffset += 60;
        volumeSlider.setPosition(Constants.SCREEN_WIDTH / 2 - volumeSlider.getWidth() / 2,
                Constants.SCREEN_HEIGHT / 2 - yOffset);
        table.addActor(volumeSlider);

        doneButton = new VisTextButton("DONE");
        doneButton.setWidth(200);
        doneButton.setHeight(50); yOffset += 60;
        doneButton.setPosition(Constants.SCREEN_WIDTH / 2 - doneButton.getWidth() / 2, Constants.SCREEN_HEIGHT / 2 - yOffset);
        doneButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {

                Preferences preferences = Gdx.app.getPreferences(Constants.GAME_NAME);
                preferences.putBoolean("sound", soundCheckbox.isChecked());
                preferences.putBoolean("music", musicCheckbox.isChecked());
                preferences.putFloat("volumeLevel", volumeSlider.getValue());
                preferences.flush();

                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen());
            }
        });
        table.addActor(doneButton);

        Gdx.input.setInputProcessor(stage);

        loadPreferences();
    }

    private void loadPreferences() {

        Preferences preferences = Gdx.app.getPreferences(Constants.GAME_NAME);
        soundCheckbox.setChecked(preferences.getBoolean("sound"));
        musicCheckbox.setChecked(preferences.getBoolean("music"));
        volumeSlider.setValue(preferences.getFloat("volumeLevel", 100));
    }

    private void checkVolumeSlider() {
        if (!soundCheckbox.isChecked() && !musicCheckbox.isChecked()) {
            volumeSlider.setDisabled(true);
        }
        else
            volumeSlider.setDisabled(false);
    }

    @Override
    public void render(float dt) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        stage.act(dt);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
