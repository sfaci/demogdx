package com.sfaci.demogdx.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.sfaci.demogdx.DemoGDX;
import com.sfaci.demogdx.util.Constants;
import net.dermetfan.gdx.scenes.scene2d.ui.Popup;

/**
 * Created by astable on 1/20/17.
 *
 * @author
 */
public class MainMenuScreen implements Screen {

    private Stage stage;

    @Override
    public void show() {

        if (!VisUI.isLoaded())
            VisUI.load();

        stage = new Stage();
        int yOffset = 0;

        VisTable table = new VisTable();
        table.setWidth(Constants.SCREEN_WIDTH);
        table.setHeight(Constants.SCREEN_HEIGHT);
        table.setFillParent(true);
        table.setPosition(0, 0);
        stage.addActor(table);

        VisTextButton playButton = new VisTextButton("PLAY");
        playButton.setWidth(250);
        playButton.setHeight(100);
        playButton.setPosition(Constants.SCREEN_WIDTH / 2 - playButton.getWidth() / 2, Constants.SCREEN_HEIGHT / 2);
        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen());
            }
        });
        table.addActor(playButton);

        VisTextButton configButton = new VisTextButton("CONFIGURATION");
        configButton.setWidth(200);
        configButton.setHeight(50); yOffset += 60;
        configButton.setPosition(Constants.SCREEN_WIDTH / 2 - configButton.getWidth() / 2, Constants.SCREEN_HEIGHT / 2 - yOffset);
        configButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new ConfigurationScreen());
            }
        });
        table.addActor(configButton);

        VisTextButton quitButton = new VisTextButton("QUIT");
        quitButton.setWidth(200);
        quitButton.setHeight(50); yOffset += 60;
        quitButton.setPosition(Constants.SCREEN_WIDTH / 2 - quitButton.getWidth() / 2, Constants.SCREEN_HEIGHT / 2 - yOffset);
        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.exit(0);
            }
        });
        table.addActor(quitButton);

        VisLabel aboutLabel = new VisLabel("Demo libGDX\n(c) Santiago Faci 2017");
        aboutLabel.setWidth(500);
        aboutLabel.setHeight(40); yOffset += 50;
        aboutLabel.setPosition(Constants.SCREEN_WIDTH / 2 - aboutLabel.getWidth() / 4, Constants.SCREEN_HEIGHT / 2 - yOffset);
        table.addActor(aboutLabel);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float dt) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        stage.act(dt);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        VisUI.dispose();
    }
}
